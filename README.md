# Space Invaders Workshop

Die Teilnehmer sollen Schritt für Schritt einen Space Invaders Clone mit Javascript auf Basis von Phaser.io
bauen.

# Schritt 0 (0.js)

Aliens und der Weltraum sind bereits vorhanden. Der Großteil der Game Logic ist ebenfalls schon vorhanden. Es
fehlt das eigene Raumschiff und die Aliens haben noch keinen Laser. Hierzu `invaders_0.js` einbinden.

# Schritt 1 (invaders_1.js)

## Eigenes Raumschiff einbauen: Folgende Funktion im `create` aufrufen.

```javascript
    function createPlayer() {
        //  The hero!
        player = game.add.sprite(400, 500, 'ship');
        player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);
    }
```

## Cursor zum Steuern benutzen:

```javascript
function() create {
    ...
    cursors = game.input.keyboard.createCursorKeys();
}

function update() {
    ...
    if (player.alive) {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);
        if (cursors.left.isDown) {
            player.body.velocity.x = -200;
        } else if (cursors.right.isDown) {
            player.body.velocity.x = 200;
        }
    }
}
```

# Schritt 2 (invaders_2.js)

## Lass dein Raumschiff seinen Laser mit `Leertaste` abfeuern:

```javascript
    function() create {
        ...
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    }

    function() update() {
        ...
        if (fireButton.isDown) {
           fireBullet();
        }
    }

    function fireBullet() {

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime) {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);

            if (bullet) {
                //  And fire it
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
            }
        }
    }

```

## Lass die Aliens sich bewegen (links nach rechts):

```javascript

    function createAliens() {
        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to({
            x: 200
        }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    }
```

# Schritt 3 (invaders_3.js)

## Zerstöre bei einem Treffer ein Alienschiff:

```javascript
    function update() {
        ...
        // Alien fires laser
        if (game.time.now > firingTimer) {
            enemyFires();
        }

        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
    }

    function collisionHandler(bullet, alien) {
        //  When a bullet hits an alien we kill them both
        bullet.kill();
        alien.kill();

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(alien.body.x, alien.body.y);
        explosion.play('kaboom', 30, false, true);

    }
```

## Lass die Aliens zufällig ihre Laser abfeuern:

```javascript
    function update() {
        ...
        // Alien fires laser
        if (game.time.now > firingTimer) {
            enemyFires();
        }
    }


    function enemyFires() {

        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);

        livingEnemies.length = 0;

        aliens.forEachAlive(function (alien) {

            // put every living enemy in an array
            livingEnemies.push(alien);
        });


        if (enemyBullet && livingEnemies.length > 0) {

            var random = game.rnd.integerInRange(0, livingEnemies.length - 1);

            // randomly select one of them
            var shooter = livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);

            game.physics.arcade.moveToObject(enemyBullet, player, 120);
            firingTimer = game.time.now + 2000;
        }
    }
```

# Schritt 4 (invaders_4.js)

## Du gewinnst, wenn alle Aliens zerstört sind

```javascript
    function collisionHandler(bullet, alien) {
        // all aliens are destroyed?
        if (aliens.countLiving() == 0) {

            enemyBullets.callAll('kill', this);
            stateText.text = " You Won, \n Click to restart";
            stateText.visible = true;

            //the "click to restart" handler
            game.input.onTap.addOnce(restart, this);
        }
    }

```

## Laser der Aliens zerstören dein Raumschiff

```javascript

    function update() {

        ...
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);

    }

    function enemyHitsPlayer(player, bullet) {

        bullet.kill();

        live = lives.getFirstAlive();

        if (live) {
            live.kill();
        }

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);

        // When the player dies
        if (lives.countLiving() < 1) {
            player.kill();
            enemyBullets.callAll('kill');

            stateText.text = " GAME OVER \n Click to restart";
            stateText.visible = true;

            //the "click to restart" handler
            game.input.onTap.addOnce(restart, this);
        }

    }
```

# More

## Dein Schiff verkraftet 3 Treffer

```javascript
function create() {
    for (var i = 0; i < 3; i++) {
        var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.4;
    }
}
```



